from flask import Flask, request, jsonify, Response
import sqlobject
import sqlobject.dbconnection
import uuid
import base64

DB_FILE = 'keys.db'

class WSWRecord(sqlobject.SQLObject):
    uuid = sqlobject.StringCol(notNone=True, length=36)
    key = sqlobject.StringCol(notNone=True)
    paid = sqlobject.BoolCol(notNone=True, default=False)

app = Flask(__name__)

@app.route('/key', methods=['GET', 'POST'])
def get_key():
    vid = request.values.get('uuid', default=None, type=str)
    if vid and len(vid) == 36:
        try:
            #檢查UUID
            vid = str(uuid.UUID(vid))
        except ValueError:
            app.logger.error('Invalid UUID recived')
            return Response(status=500)
        if request.method == 'GET':
            # 查Database
            res = list(WSWRecord.select(WSWRecord.q.uuid == vid))
            if len(res):
                if res[0].paid:
                    # 返回密鑰
                    app.logger.info('Returning key to {}'.format(res[0].uuid))
                    return Response(res[0].key, status=200)
        if request.method == 'POST':
            # 加入Database
            res = list(WSWRecord.select(WSWRecord.q.uuid == vid))
            if len(res) == 0:
                key = request.values.get('key', default=None, type=str)
                if key:
                    # 檢查密鑰
                    key_bytes = base64.urlsafe_b64decode(key)
                    if len(key_bytes) == 32:
                        if key_bytes[7] == 0x80:
                            app.logger.info('Inserting key {} from {}'.format(key, uuid))
                            WSWRecord(uuid=vid, key=key)
                            return Response(status=200)
    #有問題的請求一律回傳500
    return Response(status=500)

# 初始化數據庫# app.run()
SQLiteConnection = sqlobject.dbconnection.sqlite.builder()
sqlobject.sqlhub.processConnection = SQLiteConnection(DB_FILE)
WSWRecord.createTable(ifNotExists=True)
# 運行Flask server
app.run(host='0.0.0.0', port=2020, debug=False)