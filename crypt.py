import os
import sys
from cryptography.fernet import Fernet

# 加密
def encrypt(filename, key, ext, of=None):
    try:
        cry = Fernet(key)
        # 打開檔案
        with open(filename, 'rb') as fp:
            # 加密數據
            encrypted = cry.encrypt(fp.read())
        # 覆寫檔案
        if of:
            with open(of, 'wb') as fp:
                fp.write(encrypted)
        else:
            with open(filename + ext, 'wb') as fp:
                fp.write(encrypted)
        # 釋放記憶體
        del encrypted
    except Exception as e:
        return e

# 解密
def decrypt(filename, key, ext, of=None):
    try:
        cry = Fernet(key)
        # 打開檔案
        with open(filename, 'rb') as fp:
            # 解密數據
            decrypted = cry.decrypt(fp.read())
        # 輸出解密數據
        if of:
            with open(of, 'wb') as fp:
                fp.write(decrypted)
        else:
            with open(filename - ext, 'wb') as fp:
                fp.write(decrypted)
        # 釋放記憶體
        del decrypted
    except Exception as e:
        return e

if __name__ == '__main__':
    # 測試加密
    key = Fernet.generate_key()
    print('key:', key)
    encrypt('dummy_file', key, 'encrypted_file')

    # 測試解密
    decrypt('encrypted_file', key, 'decrypted_file')